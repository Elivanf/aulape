// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAbOHA6ErNWxibkmndpRhgqpFeiabysAoE",
    authDomain: "controle-ifgru.firebaseapp.com", 
    projectId: "controle-ifgru",
    storageBucket: "controle-ifgru.appspot.com",
    messagingSenderId: "698496928105",
    appId: "1:698496928105:web:90a93e74d3775c2a9d95f8",
    measurementId: "G-9L80F4WQWL"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
