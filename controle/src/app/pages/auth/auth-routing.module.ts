import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { LoginPage } from './../login/login.page';
import { RegisterPage } from './../register/register.page';
import { ForgotPage } from './../forgot/forgot.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  path:'', children: [
    {path:'', component: LoginPage},
    {path:'forgot', component: ForgotPage},
    {path:'register', component: RegisterPage},
  ]
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    FormsModule
  ],
  declarations:[
    LoginPage,
    ForgotPage,
    RegisterPage
  ]  
})
export class AuthRoutingModule { }
